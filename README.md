# The Fishualizer

Allows performant 3D visual interaction with large-scale neural activity data, in particular Calcium recordings from zebrafish larvae. Limited analysis capabilities are included, e.g. spike deconvolution (via [OASIS](https://github.com/j-friedrich/OASIS)). 

For details of installation and usage see below, then start fishualizing by typing `python Fishualizer.py` in the terminal. For a more complete manual, see the [WIKI](https://bitbucket.org/benglitz/fishualizer_public/wiki/Instructions).

* Current version: 16 February 2019

![icon](docs/images/GUI_Data_Screenshot.png)

# Installation guide

## Requirements
We recommend installing Python via the [Anaconda distribution](https://www.anaconda.com/download/). The following packages (and their dependencies, which are automatically installed) are required (updated 16 February 2019):

+ *python* 3.6.5 or higher
+ *numpy* (required, standard in Anaconda)
+ *scipy* (required, standard in Anaconda)
+ *h5py* (required, standard in Anaconda)
+ *pandas* (required, standard in Anaconda)
+ *imageio* (required, standard in Anaconda)
+ *matplotlib* (required, standard in Anaconda)
+ *pyqtgraph* (required, installable via Anaconda)
+ *pyopengl* (required, installable via Anaconda)
+ *psutil* 
+ *cython* 
+ *jupyter*
+ *toolz*
+ *cytoolz*
+ *qtconsole*

We developed and tested Fishualizer under Python 3.6.5 on Windows 10, Mac OS X (10.13.6) and Ubuntu 16.04

## Installation
The easiest way to get Fishualizer up and running is to install the Anaconda distribution.

A completely functional conda virtual environment can then be created using the included fish.yml file and the following command in  `{FISHUALIZEROOT}`: 
```
conda env create -f fish.yml
```
 Depending on your preferences you can also install the remaining general packages, in the environment of your choice, from the [Anaconda Navigator](https://docs.anaconda.com/anaconda/navigator/) or the command line, using either `conda` or `pip`:
```
~ $ conda install pyqtgraph
~ $ conda install pyopengl
```
Then download the *Fishualizer* repository, and place it in a convenient location on your hard disk.
For spike deconvolution, the [OASIS](https://github.com/j-friedrich/OASIS) package is included. It is automatically installed when *Fishualizer* is launched for the first time. For more information, see the documentation of [OASIS](https://github.com/j-friedrich/OASIS).

# Getting started
To run *Fishualizer*, simply type  `python Fishualizer.py` in the terminal. This will open a graphical user interface, which allows you to load and inspect your data. You can also directly pass command line options (e.g. load Data, see below). 

For testing, select File -> Open from the menu bar, and select our sample dataset (kindly provided by Misha Ahrens, Janelia Farms), located in 
 `{FISHUALIZEROOT}/Data/Data20140827_spont/SampleData.h5 `

Most options and functions are available via the menu bar and/or keyboard shortcuts (see Help->Shortcuts).

If you would like to use your own datasets, you first have to convert them to a specific hdf5 format (see below). Sample conversion script are provided in `{FISHUALIZEROOT}/Conversion/`

For a more complete manual, see the [WIKI](https://bitbucket.org/benglitz/fishualizer_public/wiki/Instructions).
