function [D,Dims] = Z_convertHDF5(varargin)

P = parsePairs(varargin);
checkField(P,'BasePath','E:\ZebrafishRecordings\');
checkField(P,'Dataset','20140820');
checkField(P,'Range',[1:100]);
checkField(P,'MicronsPerSlice',200/31); % micons per slice
checkField(P,'MicronsPerPixel',0.4); % microns per pixel
checkField(P,'ExportBehavior',1);
checkField(P,'TransposeData',1);

Dirs = dir(P.BasePath);
Recordings = {Dirs.name};
Matches = strfind(Recordings,P.Dataset);
RecInd = find(~cellfun(@isempty,Matches));
P.Dataset = Dirs(RecInd).name;
fprintf(['Selected ',P.Dataset,'\n'])
Path = [P.BasePath,P.Dataset,filesep];
tmp = load([Path,'cell_resp_dim.mat']); D.Dims = tmp.cell_resp_dim; % dimensions of the imaging data
tmp = load([Path,'cell_info.mat']);  D.Info = tmp.cell_info; % xyz coordinates of the detected cells, etc
tmp = load([Path,'frame_turn']); D.Behavior = tmp.frame_turn;  % fictive behavior data. frame_turn(:,1) = right-left turns. frame_turn(:,2) = right turns; frame_turn(:,3) = left.
if exist([Path,'Stack_frequency.txt'],'file')
  tmp = load([Path,'Stack_frequency.txt']); D.SR = tmp(1);
else 
  D.SR = 1.85;
end
D.Data = read_LSstack_fast_float([Path,'cell_resp_lowcut.stackf'],D.Dims);  % activity time series, f/f0.

% Correct Dimensions
NCells = size(D.Data,1); Locations = zeros(NCells,3);
for iC=1:NCells
  Dims(iC,:) = [...
    mean(D.Info(iC).x_minmax)*P.MicronsPerPixel,...
    mean(D.Info(iC).y_minmax)*P.MicronsPerPixel,...
    D.Info(iC).slice*P.MicronsPerSlice]/1000;
end

if ischar(P.Range) P.Range = [1:size(D.Data,2)]; end

FileName = ['E:\SampleData_',P.Dataset(1:find(P.Dataset=='_',1,'first')-1),'.h5'];
h5create(FileName,'/Data/Coordinates',[NCells,3]);
h5write(FileName,'/Data/Coordinates',Dims);
h5create(FileName,'/Data/Times',[1,length(P.Range)]);
h5write(FileName,'/Data/Times',P.Range);
h5create(FileName,'/Data/Behavior',[length(P.Range),3]);
h5write(FileName,'/Data/Behavior',D.Behavior(P.Range,1:3));
if P.TransposeData
  h5create(FileName,'/Data/Values',[length(P.Range),NCells]);
  h5write(FileName,'/Data/Values',D.Data(:,P.Range)');
else
  h5create(FileName,'/Data/Values',[NCells,length(P.Range)]);
  h5write(FileName,'/Data/Values',D.Data(:,P.Range));
end


